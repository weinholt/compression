#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2025 Vadym Kochan <vadim4j@gmail.com>

;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;; DEALINGS IN THE SOFTWARE.
#!r6rs

;; Tests tar extraction

(import (rnrs (6))
	(only (srfi :13) string-index-right)
        (srfi :64 testing)
        (compression tar)
        (compression tests runner))


(define data-path (let* ([scm-path (list-ref (command-line) 0)]
		         [last-sep (string-index-right scm-path #\/)])
		    (if last-sep
		        (string-append (substring scm-path 0 last-sep) "/data")
		        "data")))

(define (get-file-text tar-inp hdr)
  (bytevector->string
    (call-with-bytevector-output-port
      (lambda (outp)
        (extract-to-port tar-inp hdr outp)))
    (native-transcoder)))

(test-begin "check single-file TAR")
(call-with-port (open-file-input-port (string-append data-path "/hello.tar"))
  (lambda (port)
    (let ([hdr (get-header-record port)])
      (test-equal "hello.txt" (header-name hdr))
      (test-equal 'regular (header-typeflag hdr))
      (test-equal "Hello TAR\n" (get-file-text port hdr)))))
(test-end)

(test-begin "check single-file PAX")
(call-with-port (open-file-input-port (string-append data-path "/hello-pax.tar"))
  (lambda (port)
    (let ([hdr (get-header-record port)])
      (test-equal "hello-pax.txt" (header-name hdr))
      (test-equal 'regular (header-typeflag hdr))
      (test-equal "Hello PAX\n" (get-file-text port hdr)))))
(test-end)

(test-exit)
